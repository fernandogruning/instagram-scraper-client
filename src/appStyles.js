import { css } from "@emotion/core"
import { darken } from "polished";

import mediaQueries from "./utils/globalMediaQueries"

export default css(mediaQueries({
  display: `flex`,
  flexDirection: `column`,
  minHeight: `100vh`,

  '.party-popper-emoji': {
    display: `inline-block`,
    height: [`2.5rem`, `3.75rem`],
  },
}))

export const globalStyles = {
  '@font-face': {
    fontFamily: `"Product Sans"`,
    src: `url('/fonts/Product Sans Regular.ttf') format('truetype')`,
    fontWeight: 400,
    fontStyle: `normal`,
  },

  html: {
    '@font-face': {
      fontFamily: `"Product Sans"`,
      src: `url('/fonts/Product Sans Bold.ttf') format('truetype')`,
      fontWeight: 700,
      fontStyle: `normal`,
    }
  },

  '*, *:before, *:after': {
    boxSizing: `border-box`,
    fontFamily: `'Product Sans', Helvetica, Arial, sans-serif`,
  },

  body: {
    margin: 0,
    fontSize: `1rem`,
    color: `hsl(0, 13%, 9%)`,
  },

  a: {
    color: `tomato`,

    '&:hover': {
      color: darken('0.08', 'tomato')
    }
  },

  h1: mediaQueries({
    marginTop: 0,
    fontSize: [`2.75rem`, `3.75rem`]
  }),

  h3: {
    marginTop: 0,
    fontSize: `1.5rem`,
  },

  p: {
    marginTop: 0,
  },

  main: {
    maxWidth: `1056px`,
    width: `100%`,
    margin: `2rem auto 0`,
    padding: `0 1rem`,
  },
}
