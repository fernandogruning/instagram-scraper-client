import facepaint from 'facepaint';

const breakpoints = [576, 768, 992, 1200]

const mediaQueries = facepaint(
  breakpoints.map(breakpoint => `@media (min-width: ${breakpoint}px)`)
)

export default mediaQueries
