const getDownloadURL = ({ allMedia, mediaType, id }) => {
  if (mediaType === 'carousel') {
    return `https://scrape-a-gram.fernandogruning.com/download?id=${id}&${allMedia.map(({ extractedMedia }) => `url=${encodeURIComponent(extractedMedia)}`).join("&")}`
  } else {
    return `https://scrape-a-gram.fernandogruning.com/download?id=${id}&${allMedia.map((extractedMedia) => `url=${encodeURIComponent(extractedMedia)}`).join("&")}`
  }
}

export default getDownloadURL

