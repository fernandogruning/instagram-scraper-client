/** @jsx jsx */
import { jsx } from "@emotion/core"

import formControlStyles from "./styles"

const FormControl = ({ type, formControlRef, ...otherProps }) => (
  <input type={type} css={formControlStyles} ref={formControlRef} {...otherProps} />
)

export default FormControl
