import { css } from "@emotion/core"
import { rgba } from "polished"

import mediaQueries from "../../utils/globalMediaQueries"

export default css(mediaQueries({
  display: `block`,
  width: `100%`,
  padding: [`0.5rem`, `0.5rem 1rem`],
  backgroundColor: rgba('#000', .04),
  border: `1px solid ${rgba('#000', 0)}`,
  borderRadius: `.5rem`,
  fontSize: [`1rem`, `1.25rem`],

  '&:focus': {
    outline: `none`,
    backgroundColor: `#fff`,
    border: `1px solid ${rgba('#000', 0.12)}`,
  }
}))
