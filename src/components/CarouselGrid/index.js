/** @jsx jsx */
// import React from "react"
import { jsx } from "@emotion/core"

import carouselGridStyles from './styles';

const CarouselGrid = (props) => {
  const { itemsData } = props

  let numberOfRows = 1
  if (itemsData.length === 2 || itemsData.length === 4) {
    numberOfRows = 2
  } else if (itemsData.length >= 3) {
    numberOfRows = 3
  }

  const renderItems = (itemsData) => itemsData.map(itemData => (
    <a
      key={itemData.extractedMedia}
      className={`grid-item ${itemData.mediaType}`}
      href={itemData.extractedMedia}
      target="_blank"
      rel="noopener noreferrer"
    >
      <img src={itemData.mediaType === 'video' ? itemData.videoThumbnail : itemData.extractedMedia} alt="" />
    </a>
  ))

  return (
    <div css={carouselGridStyles(numberOfRows)}>
      {renderItems(itemsData)}
    </div>
  )
}

export default CarouselGrid