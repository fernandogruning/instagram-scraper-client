import { css } from '@emotion/core'
import { rgba } from 'polished';

export default count => css({
  display: `grid`,
  gridTemplateColumns: `repeat(${count}, 1fr)`,
  gridGap: `0.5rem`,

  '.grid-item': {
    position: `relative`,
    display: `block`,
    width: `100%`,
    height: 0,
    paddingTop: `100%`,

    '&:after': {
      zIndex: 2,
      content: `"View original"`,
      position: `absolute`,
      top: 0,
      left: 0,
      display: `flex`,
      alignItems: `center`,
      justifyContent: `center`,
      width: `100%`,
      height: `100%`,
      backgroundColor: rgba('black', 0),
      opacity: 0,
      fontSize: `1.25rem`,
      lineHeight: `1.25rem`,
      textAlign: `center`,
      color: `white`,
      transition: `background-color 0.2s, opacity 0.2s`
    },

    '&:hover': {
      '&:after': {
        backgroundColor: rgba('black', 0.6),
        opacity: 1
      }
    },

    '&.video': {
      '&:before': {
        zIndex: 1,
        content: `""`,
        position: `absolute`,
        top: `1rem`,
        right: `1rem`,
        display: `block`,
        width: `1.25rem`,
        height: `1.25rem`,
        backgroundImage: `url('/images/play-icon.svg')`,
        backgroundRepeat: `none`,
        backgroundPosition: `center center`,
        backgroundSize: `100% 100%`,
        filter: `invert(100%)`
      }
    },

    '*': {
      position: `absolute`,
      top: 0,
      left: 0,
      display: `block`,
      width: `100%`,
      height: `100%`,
      objectFit: `cover`
    }
  }
})
