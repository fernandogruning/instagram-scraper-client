/** @jsx jsx */
import { jsx } from "@emotion/core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";

import Card from "../Card"
import errorStyles from "./styles"

const ErrorMessage = (props) => (
  <Card newStyles={errorStyles}>
    <FontAwesomeIcon icon={faTimesCircle} transform="grow-4" color="tomato" className="close-icon" />{props.error || "Default error, please ignore this"}
  </Card>
)

export default ErrorMessage
