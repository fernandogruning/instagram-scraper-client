import { css } from "@emotion/core"

export default css({
  display: `inline-flex`,
  alignItems: `center`,
  padding: `0.5rem .5625rem`,
  borderRadius: 9999,
  marginTop: `1.25rem`,
  fontWeight: 700,
  boxShadow: `
    0 3px 1px -2px rgba(0,0,0,.2),
    0 2px 2px 0 rgba(0,0,0,.14),
    0 1px 5px 0 rgba(0,0,0,.12)
  `,

  '.close-icon': {
    marginRight: `0.625rem`
  }
})
