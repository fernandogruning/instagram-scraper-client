/** @jsx jsx */
import React, { Component } from "react"
import { jsx } from "@emotion/core"

import formStyles, { invalidUrlStyles } from "./styles"
import FormControl from "../FormControl"
import Button from "../Button"

class URLForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      url: "",
      isInvalid: false
    }

    this.inputRef = React.createRef()

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleFocus = this.handleFocus.bind(this)
  }

  componentDidMount() {
    this.inputRef.current.focus()
    this.setState({ url: this.props.submittedUrl || "", isInvalid: this.props.invalidUrl })
  }

  handleChange(e) {
    // this.setState({ currentValue: e.target.value })
    // this.props.resetErrorHandler()
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault()

    this.props.onFormSubmit(this.state)
    this.props.resetErrorHandler()
  }

  handleFocus() {
    this.setState({ isInvalid: false })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} css={[formStyles, this.state.isInvalid && invalidUrlStyles]}>
        <h3>
          <label htmlFor="url-input">Instagram URL</label>
        </h3>

        <FormControl
          type="text"
          name="url"
          placeholder="https://instagram.com/p/"
          required="required"
          id="url-input"
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          value={this.state.url}
          formControlRef={this.inputRef}
        />
        <p className="helper-text">Paste an Instagram URL above to get the post image or video</p>

        <Button type="submit" primary={true ? 1 : 0}>Enviar</Button>

        {this.state.loading && "Loading..."}
      </form>
    )
  }
}

export default URLForm
