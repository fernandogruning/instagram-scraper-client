import { css } from "@emotion/core"
import { darken } from "polished"

import mediaQueries from '../../utils/globalMediaQueries';

export default css(mediaQueries({
  h3: {
    marginBottom: `0.5rem`,
  },

  '#url-input': {
    marginBottom: `0.25rem`
  },

  '.helper-text': {
    color: `#666`,
    fontSize: [`0.75rem`, `inherit`]
  }
}))

export const invalidUrlStyles = css({
  '#url-input': {
    border: `1px solid ${darken(0.08, `tomato`)}`,
    color: darken(0.08, `tomato`)
  }
})
