/** @jsx jsx */
import { jsx } from "@emotion/core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCopyright } from "@fortawesome/free-regular-svg-icons";

import footerStyles from "./styles"

const currentYear = new Date().getFullYear()

const Footer = () => (
  <footer css={footerStyles}>
    <div>
      <FontAwesomeIcon icon={faCopyright} /> {currentYear}. Made with <span role="img" aria-label="love">❤️</span> by <a href="https://fernandogruning.com" target="_blank" rel="noopener noreferrer">Fernando Gruning</a>
    </div>
  </footer>
)

export default Footer
