import { css } from '@emotion/core';

export default css({
  display: `flex`,
  justifyContent: `flex-end`,
  padding: `0 1rem`,
  paddingTop: `2rem`,
  marginTop: `auto`,
  marginBottom: `0.5rem`,
  // fontSize: `0.75rem`,
  fontSize: `14px`,
})
