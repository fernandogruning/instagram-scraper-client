/** @jsx jsx */
// import React from "react"
import { jsx } from "@emotion/core"

import buttonStyles, { buttonPrimaryStyles, buttonSecondaryStyles } from "./styles";

const Button = (props) => {
  const { href, primary, secondary, newStyles, children, ...rest } = props

  if (props.href) {
    return (
      <a href={href} css={[buttonStyles, primary && buttonPrimaryStyles, secondary && buttonSecondaryStyles, newStyles && newStyles]} {...rest}>{children}</a>
    )
  } else {
    return (
      <button css={[buttonStyles, primary && buttonPrimaryStyles, secondary && buttonSecondaryStyles, newStyles && newStyles]} {...rest}>{children}</button>
    )
  }
}

export default Button
