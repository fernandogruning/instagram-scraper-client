import { css } from "@emotion/core"
import { darken } from "polished"

const buttonStyles = css({
  display: `inline-block`,
  padding: `0.5rem 1.5rem`,
  border: `none`,
  borderRadius: `.5rem`,
  fontSize: `1rem`,
  fontWeight: 700,
  letterSpacing: `0.8px`,
  textDecoration: `none`,
  cursor: `pointer`,

  '&:focus': {
    outline: `none`,
  },
})

export default buttonStyles

export const buttonPrimaryStyles = css({
  background: `tomato`,
  border: `1px solid tomato`,
  color: `#fff`,

  '&:hover': {
    backgroundColor: darken(0.08, `tomato`),
    border: `1px solid ${darken(0.08, `tomato`)}`,
    color: `#fff`
  }
})

export const buttonSecondaryStyles = css({
  padding: `0.5rem 0`,
  background: `#fff`,
  color: `#aaa`,

  '&:hover': {
    color: darken(0.16, `#aaa`),
  }
})
