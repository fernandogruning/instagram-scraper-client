/** @jsx jsx */
// import React from "react"
import { jsx } from "@emotion/core"

import cardStyles, { cardMediaStyles } from "./styles"

const Card = (props) => (
  <div css={[cardStyles, props.cardMedia && cardMediaStyles, props.newStyles && props.newStyles]}>
    {props.cardHeader ? props.cardHeader : null}
    {props.cardBody ? props.cardBody : null}
    {props.children}
  </div>
)

export default Card
