import { css } from "@emotion/core"

import mediaQueries from '../../utils/globalMediaQueries';

export default css({
  maxWidth: `100%`,
  borderRadius: `.5rem`,
  boxShadow: `
    0 5px 5px -3px rgba(0,0,0,.2),
    0 8px 10px 1px rgba(0,0,0,.14),
    0 3px 14px 2px rgba(0,0,0,.12)
  `,

  ".card-body": mediaQueries({
    padding: [`1rem`, `2rem`]
  })
})

export const cardMediaStyles = css(mediaQueries({
  marginTop: `2rem`,
  marginBottom: `2rem`,

  ".card-header": {
    display: `flex`,
    alignItems: `center`,
    padding: `0.75rem 1rem`,
  },

  ".card-body": {
    minHeight: [`0`, `${60 / 1.91}rem`],
    padding: [`0`, `0 2rem 3rem 2rem`]
  },

  ".btn-group": {
    display: `flex`,
    alignItems: `center`,
    marginLeft: `auto`,

    "& > *:not(:last-child)": {
      marginRight: `1.5rem`,
    },

    '& > *:first-of-type': {
      span: {
        display: [`none`, `inline`]
      }
    }
  },

  ".card-image, .card-video": {
    display: `block`,
    maxWidth: [`100%`, `60%`],
    height: `auto`,
    margin: `0 auto`,
    borderBottomLeftRadius: [`0.5rem`, 0],
    borderBottomRightRadius: [`0.5rem`, 0]
  }
}))
