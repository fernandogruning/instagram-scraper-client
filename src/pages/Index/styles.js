import { css } from "@emotion/core"

export default css({
  h1: {
    marginBottom: `2rem`
  }
})
