/** @jsx jsx */
import { jsx } from "@emotion/core"

import indexStyles from "./styles"
import Card from "../../components/Card"
import URLForm from "../../components/URLForm"

const Index = (props) => {
  const CardBody = () => (
    <div className="card-body">
      <URLForm postCaller={props.postCaller} onFormSubmit={props.onFormSubmit} invalidUrl={props.invalidUrl} submittedUrl={props.submittedUrl} resetErrorHandler={props.resetErrorHandler} />
    </div>
  )

  return (
    <main css={indexStyles}>
      <h1>Scrape-a-Gram</h1>

      <Card cardBody={<CardBody />} />
      {props.children}
    </main>
  )
}

export default Index
