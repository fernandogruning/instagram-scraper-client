import { css } from "@emotion/core"

export default css({
  h1: {
    marginBottom: 0
  },

  h3: {
    color: `#777777`
  },

  '.skeleton-wrapper': {
    maxWidth: `40rem`,
    height: `40rem`,
    margin: `0 auto`
  }
})
