/** @jsx jsx */
import { jsx } from "@emotion/core"
import Skeleton from "react-loading-skeleton"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faExternalLinkSquareAlt } from "@fortawesome/free-solid-svg-icons";

import resultStyles from "./styles"
import Card from "../../components/Card";
import Button from "../../components/Button";
import CarouselGrid from '../../components/CarouselGrid'
import getDownloadURL from "../../utils/getDownloadURL"

const Result = (props) => {
  const CardHeader = () => (
    <div className="card-header">
      {!props.loading ?
        <Button secondary={true ? 1 : 0} onClick={props.resetStateHandler}>
          <FontAwesomeIcon icon={faAngleLeft} transform="grow-4" /> Back
        </Button> :
        <Skeleton width={52} />
      }

      <div className="btn-group">
        {!props.loading ?
          <Button
            secondary={true ? 1 : 0}
            href={props.submittedUrl}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span>Go to post</span> <FontAwesomeIcon icon={faExternalLinkSquareAlt} />
          </Button> :
          <Skeleton width={104} />
        }
        {!props.loading ?
          <Button
            primary={true ? 1 : 0}
            href={getDownloadURL({
              allMedia: props.allMedia,
              mediaType: props.mediaType,
              id: props.mediaId
            })}
          >
            Download {props.mediaType}
          </Button> :
          <Skeleton width={139} height={38} />
        }
      </div>
    </div>
  )

  const CardBody = () => {
    let cardBodyContent = (
      <div className="skeleton-wrapper">
        <Skeleton className="skeleton" height="100%" />
      </div>
    )

    if (props.mediaType === 'image') {
      cardBodyContent = <img src={props.allMedia[0]} className="card-image" alt="" />
    }

    if (props.mediaType === 'video') {
      cardBodyContent = <video src={props.allMedia[0]} controls className="card-video"></video>
    }

    if (props.mediaType === 'carousel') {
      cardBodyContent = <CarouselGrid itemsData={props.allMedia} />
    }

    return (
      <div className="card-body">
        {cardBodyContent}
      </div>
    )
  }

  return (
    <main css={resultStyles}>
      {props.mediaType ?
        <h1>Here is your {props.mediaType}! <img src="/images/party-popper.png" alt="" className="party-popper-emoji" /></h1> :
        <h1>Fetching your post...</h1>
      }
      {props.mediaType ?
        <h3>Enjoy!</h3> :
        <h3>Hang tight!</h3>
      }

      <Card cardMedia cardHeader={<CardHeader />} cardBody={<CardBody />} />
    </main>
  )
}

export default Result
