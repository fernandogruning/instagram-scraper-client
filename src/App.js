/** @jsx jsx */
import { Component } from 'react'
import { Global, jsx } from '@emotion/core'
import ReactGA from 'react-ga';

import appStyles, { globalStyles } from "./appStyles"
import Index from "./pages/Index"
import Result from "./pages/Result"
import ErrorMessage from './components/ErrorMessage';
import Footer from "./components/Footer"

// Analytics initialization
ReactGA.initialize('UA-112480106-2')
ReactGA.pageview(window.location.pathname + window.location.search)

const initialState = {
  loading: false,
  currentPage: 'index',
  submittedUrl: "",
  id: "",
  allMedia: [],
  mediaType: "",
  error: null,
}

class App extends Component {
  constructor(props) {
    super(props)

    this.state = initialState

    this.handleFormSubmit = this.handleFormSubmit.bind(this)
    this.resetError = this.resetError.bind(this)
    this.resetState = this.resetState.bind(this)
  }

  handleFormSubmit(objUrl) {
    this.setState({ loading: true, currentPage: 'result' })
    this.getMedia(objUrl)
      .then(res => {
        const parsedResponse = JSON.parse(res)

        if (parsedResponse.error) {
          this.setState({ currentPage: 'index', ...parsedResponse })
          return
        }
        this.setState({ loading: false, ...parsedResponse })
      }).catch(error => {
        console.log(error)
        this.setState({ error, loading: false })
      })
  }

  async getMedia(data) {
    const response = await fetch('https://scrape-a-gram.fernandogruning.com/scrape-gram', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    const body = await response.text()

    return body
  }

  resetError() {
    this.setState((state, props) => {
      if (state.error !== null) {
        return { error: null }
      }
    })
  }

  resetState() {
    this.setState({ ...initialState })
  }

  componentDidMount() {
    const parsedUrl = new URL(window.location)
    const igShareQuery = parsedUrl.searchParams.get('text')

    if (igShareQuery) {
      this.handleFormSubmit({ url: igShareQuery })
    }
  }

  render() {
    let currentPage = (
      <Index
        onFormSubmit={this.handleFormSubmit}
        invalidUrl={this.state.error && this.state.submittedUrl}
        submittedUrl={this.state.submittedUrl}
        resetErrorHandler={this.resetError}
      >
        {this.state.error && <ErrorMessage error={this.state.error} />}
      </Index>
    )

    if (this.state.currentPage === 'result') {
      currentPage = (
        <Result
          mediaId={this.state.id}
          mediaType={this.state.mediaType}
          allMedia={this.state.allMedia}
          submittedUrl={this.state.submittedUrl}
          resetStateHandler={this.resetState}
          loading={this.state.loading}
        />
      )
    }

    return (
      <div>
        <Global styles={globalStyles} />

        <div css={appStyles}>
          {currentPage}
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;
